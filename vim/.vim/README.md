# Vim Configs

Here are my vim configs. Do what you want with them.

# Manual install instructions

Compile vimproc

    cd bundle/vimproc.vim && make

Compile YouCompleteMe

    cd bundle/YouCompleteMe/ && ./install.sh

Install Tern Dependencies

    cd bundle/tern_for_vim/ && npm install

