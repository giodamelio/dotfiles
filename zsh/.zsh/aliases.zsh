# ls things
alias ls='ls --color=always'
alias la='ls -ah'
alias ll='ls -lah'

# Program replacements
alias vim='NVIM_TUI_ENABLE_TRUE_COLOR=1 nvim'
alias top='htop'

# Shortcuts
alias r='ranger'
alias dmesg='dmesg --color=always | less -r'
alias npmbin='source npmbin'
