# Very basic prompt
export PS1="[\u@\h]-[\w]-[\\$]  "

# Load nvm
export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"
